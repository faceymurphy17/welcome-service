package jm.edu.utech.ap.welcomelib;

/**
 * 
 * */
public interface IWelcomeService {
	
	String DEFAULT_WELCOME_PHRASE 
		= "Thank you for coming ";

	/**
	 * Prepares and returns a welcome message
	 * @param name Name of person to welcome 
	 * */
	String getWelcomeMessgae(String name);
}
