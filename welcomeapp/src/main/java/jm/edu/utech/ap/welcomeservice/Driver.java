package jm.edu.utech.ap.welcomeservice;


import java.util.Scanner;

import jm.edu.utech.ap.welcomelib.IWelcomeService;
import jm.edu.utech.ap.welcomeservice.WelcomeService;

public class Driver {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		IWelcomeService welcomeService = new WelcomeService();
		System.out.println("Enter your name: ");
		System.out.println(welcomeService.getWelcomeMessgae(input.nextLine()));
		input.close();
	}

}
