package jm.edu.utech.ap.welcomeservice;

import jm.edu.utech.ap.welcomelib.IWelcomeService;

public class WelcomeService 
	implements IWelcomeService
{

	public String getWelcomeMessgae(String name) {
		return IWelcomeService.DEFAULT_WELCOME_PHRASE + name;
	}

}
